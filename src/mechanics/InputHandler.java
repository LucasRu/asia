package mechanics;

import audio.AudioObserver;
import javafx.scene.input.KeyCode;
import entities.Model;

public class InputHandler {

    private Model model;
    static AudioObserver audioObserver = new AudioObserver();

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keyCode) {
        keyHandler(keyCode, true);
    }

    public void onKeyReleased(KeyCode keyCode) {
        keyHandler(keyCode, false);
    }

    public void keyHandler(KeyCode keyCode, boolean setter) {
        switch (keyCode) {
            case UP:
                model.getPlayer().movementKeys.put('u', setter);
                facingDirection('u');
                break;
            case DOWN:
                model.getPlayer().movementKeys.put('d', setter);
                facingDirection('d');
                break;
            case LEFT:
                model.getPlayer().movementKeys.put('l', setter);
                facingDirection('l');
                break;
            case RIGHT:
                model.getPlayer().movementKeys.put('r', setter);
                facingDirection('r');
                break;
            case SPACE:
                model.getPlayer().movementKeys.put(' ', setter);
                attacking();
                if (model.getPlayer().canAttack) {
                    audioObserver.onHit();
                }
                model.getPlayer().canAttack = false;

                break;
            case SHIFT:
                model.getPlayer().movementKeys.put('s', setter);
                isRunning(true);
                break;
            case ENTER:
                model.getPlayer().movementKeys.put('e', setter);
                break;
            case NUMPAD1:
                model.getPlayer().invKeys.put(1, setter);
                break;
            case NUMPAD2:
                model.getPlayer().invKeys.put(2, setter);
                break;
            case NUMPAD3:
                model.getPlayer().invKeys.put(3, setter);
                break;
            case NUMPAD4:
                model.getPlayer().invKeys.put(4, setter);
                break;
            case NUMPAD5:
                model.getPlayer().invKeys.put(5, setter);
                break;
            case NUMPAD6:
                model.getPlayer().invKeys.put(6, setter);
                break;

        }
    }

    public void isRunning(boolean value) {
    }

    public void facingDirection(char direction) {
        model.getPlayer().facing = direction;
    }

    public void attacking() {
        model.getPlayer().isAttacking = true;
    }
}
