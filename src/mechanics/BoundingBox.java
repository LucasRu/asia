package mechanics;

import entities.Player;


public class BoundingBox {

    private int minX, maxX, minY, maxY;
    private boolean collidingNorth, collidingEast, collidingSouth, collidingWest;

    public BoundingBox(int minX, int maxX, int minY, int maxY){
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
    }

    public void collidingSides(Player player, BoundingBox other){
        if(maxX >= other.minX) { collidingEast = true; }
        else { collidingEast = false; }
        if(minX <= other.maxX) { collidingWest = true; }
        else { collidingWest = false; }
        if(maxY >= other.minY) { collidingSouth = true; }
        else { collidingSouth = false; }
        if(minY <= other.maxY) { collidingNorth = true; }
        else { collidingNorth = false; }

        boolean[] abilities = {collidingNorth, collidingEast, collidingSouth, collidingWest};
        player.movementAbility.put('u' , !collidingNorth);
        player.movementAbility.put('r' , !collidingEast);
        player.movementAbility.put('d' , !collidingSouth);
        player.movementAbility.put('l' , !collidingWest);
        //Player.setMovementAbility(abilities);

    }

    public boolean wouldCollide(BoundingBox other){
        return maxX +20 >= other.minX && minX-20 <= other.maxX
                && maxY+20 >= other.minY && minY-20 <= other.maxY;
    }

    public boolean isColliding(BoundingBox other){

        return maxX-20 >= other.minX && minX+10 <= other.maxX
                && maxY-20 >= other.minY && minY <= other.maxY;
    }
}
