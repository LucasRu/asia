package mechanics;

public class Terrain {

    private int x, y;
    private int height, width;
    private int hp;
    public boolean isAttacking = false;
    public boolean damaged;
    public boolean canAttack;
    public long attackControl = 3000;
    public long deathTimer = 1000;
    public boolean vanish = false;

    public Terrain(int x, int y) {
        this.x = x;
        this.y = y;
        this.height = 32;
        this.width = 24;
        this.hp = 8;
    }

    public void setCD() {
        this.attackControl = 1500;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public void attack() {
        this.isAttacking = true;
    }

    public void canAttack() {
        this.canAttack = true;
    }

    public void stopAttack() {
        this.isAttacking = false;
    }

    public BoundingBox boundingBox() {
        return new BoundingBox(x, x + width, y, y + height);
    }

    public void remove() {
        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;
    }

    public boolean died() {
        if (hp <= 0) {
            return true;
        }
        return false;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void damaged(int dmg) {
        hp -= (4 + dmg);
        damaged = true;
    }

}
