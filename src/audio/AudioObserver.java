package audio;

import audio.Audio;

import java.util.Random;

public class AudioObserver  {

	public void onComplete()  {

	}

	public void onBossSpawn() {
		Audio.playSoundOnce("siren.wav");

	}

	public void playerDead()  {
		Audio.playSoundOnce("explosion.wav");
	}

	public void onHit()  {
		Random random = new Random();
		int sound = random.nextInt(4);
		switch (sound){
			case 1: Audio.playSoundOnce("attack1.wav"); break;

			case 2: Audio.playSoundOnce("attack2.wav"); break;

			case 3: Audio.playSoundOnce("attack3.wav"); break;
		}

	}

	public void playTheme()  {
		Audio.playSoundOnce("OOT.wav");
	}


}
