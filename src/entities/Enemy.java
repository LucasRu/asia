package entities;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Enemy {

    private int x, y;
    private int height, width;
    private int hp;
    private float speedX=0.64f;
    private Random random = new Random();
    public boolean isAttacking = false;
    public boolean isGettingHit = false;
    public boolean isDying = false;

    public Enemy(int x, int y){
        this.x=x;
        this.y=y;
        this.height = 42;
        this.width = 42;
        this.hp=10;
    }

    public void die(){
        this.y=random.nextInt(1000);
        this.x=random.nextInt(800);
    }

    public int getHeight(){
        return this.height;
    }

    public int getWidth(){
        return this.width;
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }


    public void move(int dx, int dy){
        this.x += dx;
        this.y += dy;
    }

    public void update(long elapsedTime){
        this.x = Math.round(this.x + elapsedTime * this.speedX);

    }

    public void attack(){
        isAttacking=true;
    }

    public void getDamage(){
        this.hp-=5;
        isGettingHit=true;
        if(this.hp<=0){this.isDying=true;}
    }
}
