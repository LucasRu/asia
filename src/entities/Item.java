package entities;

import mechanics.BoundingBox;

public class Item {

    private boolean inInventory, used;
    private int x, y;
    private int w, h;

    private int dX, dY;
    private int dW, dH;
    private int invW, invH;
    private int invNumber;
    private static int itemCount=1;
    private String type;
    private static int invSpacing = 25;
    public boolean dropped;

    public Item(int x, int y){
        this.dropped = false;
        this.invH = 0;
        this.invW = 0;

        this.dX = x;
        this.dY = y;
        this.dH = 20;
        this.dW = 20;
        this.type = type;

        this.x = 1500 + invSpacing;
        this.y = 65;
        if (itemCount==3) { this.y += 25; this.x = 1500 + invSpacing; invSpacing = 25; }
        if (itemCount>3) { this.x = 1525 + invSpacing; }
        this.w = 30;
        this.h = 30;
        itemCount++;
        invSpacing+=25;
        inInventory = false;
        used = false;
    }

    public int getInvW(){
        return this.invW;
    }

    public int getInvH(){
        return this.invH;
    }

    public int getX(){
        return this.x;
    }


    public BoundingBox boundingBox() {
        return new BoundingBox(x, x + w -20, y, y + h -20);
    }

    public void pickedUp(){
        inInventory = true;
    }

    public String getType(){
        return this.type;
    }

    public void used(){
        used = true;
        this.h = 0;
        this.w = 0;
    }

    public boolean wasPickedUp() {
        return this.inInventory;
    }

    public boolean wasUsed() {
        return this.used;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public int getdX() {
        return dX;
    }

    public int getdY() {
        return dY;
    }

    public int getdH() {
        return dH;
    }

    public int getdW() {
        return dW;
    }
}
