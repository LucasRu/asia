package entities;

import audio.AudioObserver;
import mechanics.BoundingBox;

import java.util.HashMap;

public class Player {
    private int x;
    private int y;
    public int h;
    private int w;
    public int hp;
    public boolean canAttack = true;
    public boolean canGetDamage = true;
    public boolean isRunning=false;
    public char facing;
    public String secondDirection="";
    public boolean isAttacking=false;
    private Character[] keys = {'u' ,'l' ,'d' ,'r' ,'s', ' ', 'e', '1', '2', '3', '4', '5', '6'};
    public int[] inventoryKeys = {1, 2, 3, 4, 5, 6};
    public HashMap<Character, Boolean> movementKeys = new HashMap<>();
    public HashMap<Integer, Boolean> invKeys = new HashMap<>();
    public HashMap<Character, Boolean> movementAbility = new HashMap<>();
    static AudioObserver audioObserver = new AudioObserver();
    private int coinCount, armor, damage, armorCount, damageCount;

    public Player(int x, int y) {
        this.h = 44;
        this.w = 44;
        this.x = x ;
        this.y = y ;
        this.hp = 12;
        this.facing = 'd';
        this.coinCount = 0;
        this.armor = 0;
        this.damage = 0;
        fillKeyObserver();
    }

    public String getCoinCount(){
        return "0" + this.coinCount;
    }

    public int getHp(){
        return this.hp;
    }

    public char getFacing(){
        return this.facing;
    }

    public void getDamage(){
        if(!(armor>0)){ this.hp -= 1; }
        armor--;
    }

    public int getDmgOutput(){
        return this.damage;
    }

    private void fillKeyObserver(){
        for ( Character key : keys ){
            movementKeys.put(key, false);
            movementAbility.put(key, true);
        }

        for ( Integer i : inventoryKeys ){
            invKeys.put(i, false);
        }
    }

    public void increaseArmor(){
        this.armor = 2;
        System.out.println("Armor increased");
    }

    public void increaseDamage(){
        this.damage += 4;
        this.damageCount = 2;
        System.out.println("Damage increased");
    }

    public void collectCoin(){
        this.coinCount ++;
        System.out.println("Coin collected");
    }

    public BoundingBox boundingBox() {
        return new BoundingBox(x, x + w, y, y + h);
    }

    private void assistanceSetter(boolean[] abilities){

    }

    public static void setMovementAbility(boolean[] abilities){

    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public void move(int dx, int dy){
        if(movementKeys.get('s')){
            this.x += dx * 4;
            this.y += dy * 4;
        }
        else{
            this.x += dx;
            this.y += dy;
        }
    }

    public void damaged(){
        this.hp -= 10;
        if(this.hp<=0){
            playerDies();
        }
    }

    public void attack(){
        isAttacking=true;
        if (damageCount>0){ damage=0; damageCount--; }
    }

    public void stopAttacking(){
        isAttacking=false;
    }

    public void healed(){
        this.hp = 8;
    }

    public void playerDies(){
        System.out.println("game.Game Over");
    }
}
