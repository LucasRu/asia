package entities;//
import audio.AudioObserver;
import mechanics.Terrain;

import java.util.*;
import java.util.List;

public class Model {
    private Player player;
    public double WIDTH = 1900;
    public double HEIGHT = 1000;
    public List<Terrain> terrains = new ArrayList<>();
    private Random random = new Random();
    private List<Enemy> enemies = new LinkedList<>();
    private int calcX = 0;
    private int calcY = 0;
    private HashMap<Integer, String> itemDrop = new HashMap<>();
    private HashMap<Item, Integer> inventoryMap = new HashMap<>();
    private int inventoryCounter;
    private HashMap<Integer, Boolean> inventoryStatus = new HashMap<>();
    private HashMap<Integer, Item> inventoryMapping = new HashMap<>();
    public HashMap<Item, String> itemMap;
    private int secCounter=1500, secCounter2=1500, secCounter3=1500;
    private AudioObserver audioObserver;

    public HashMap<Integer, Item> potions = new HashMap<>();


    public Model() {
        itemMap = new HashMap<>();
        itemDrop.put(1, "Potion");
        itemDrop.put(2, "Armor");
        itemDrop.put(3, "Weapon");
        itemDrop.put(4, "Coin");
        this.inventoryCounter = 1;



        for(int i = 1; i<=6; i++){
            inventoryStatus.put(i, false);
        }

        this.player = new Player(1630,570);

        for (int i = 0; i < 6; i++) {
            Terrain terrain = new Terrain(random.nextInt(1000)+200, 480 );
            this.terrains.add(terrain);
        }


        for (int i = 0; i < 0; i++){
            this.enemies.add(new Enemy(1100, 500));
        }

            for (Terrain terrain : terrains){
                Item potion = new Item(terrain.getX(), terrain.getY());
                potions.put(terrain.getX(), potion);
                int item = random.nextInt(5);

                switch (item){
                    case 1 : itemMap.put(potion, "Potion"); break;
                    case 2 : itemMap.put(potion, "Weapon"); break;
                    case 3 : itemMap.put(potion, "Armor"); break;
                    case 4 : itemMap.put(potion, "Coin"); break;
                }

            }


    }


    public List<Enemy> getEnemies() {
        return this.enemies;
    }

    public List<Terrain> getTerrains() {
        return this.terrains;
    }

    public Player getPlayer() {
        return player;
    }

    public void update(long elapsedTime) {
        boolean resetMovement=false;

        secCounter -= elapsedTime;
        if(secCounter <= 0){
            player.canAttack=true;
            player.canGetDamage=true;
            secCounter = 1000;
        }

        for (Terrain terrain : terrains){
            if(terrain.died()){
                terrain.deathTimer -= elapsedTime;
                if(terrain.deathTimer <= 0){
                    terrain.vanish = true;
                }
            }
        }


        for(Terrain terrain : terrains){
            terrain.attackControl -= elapsedTime;
            if(terrain.attackControl <= 0){
                terrain.canAttack = true;
                terrain.attackControl = 3000;
            }
        }

        for ( Terrain terrain : terrains) {
            if(player.boundingBox().wouldCollide(terrain.boundingBox())){
                if(player.canGetDamage && terrain.canAttack) { terrain.attack(); player.getDamage();

                terrain.canAttack=false; player.canGetDamage = false; }
                if(player.movementKeys.get(' ')){
                        if(player.canAttack){
                                terrain.damaged(player.getDmgOutput());
                                player.canAttack = false;
                            if(terrain.died()){
                                potions.get(terrain.getX()).dropped = true;
                            }
                        }
            }
            }
            else {
                terrain.stopAttack();
            }
        }

        int test = 1;

        for( Item item : potions.values()){

            if ( player.movementKeys.get('e') == true && item.dropped ){
                    item.pickedUp();
                    for(Integer i : inventoryStatus.keySet()){
                        if(!inventoryStatus.get(i)){
                            inventoryStatus.put(i, true);
                            inventoryMapping.put(i, item);
                            inventoryMap.put(item, i);
                            test = i;
                            break;
                        }
                    }
                }

            if(inventoryMap.containsKey(item) && itemMap.containsKey(item)){
                if( player.invKeys.get(inventoryMap.get(item)) && !(item.wasUsed())){
                    item.used();
                    inventoryStatus.put(test, false);
                    switch (itemMap.get(item)){
                        case ("Potion"): player.healed(); break;
                        case ("Armor"): player.increaseArmor(); break;
                        case ("Weapon"): player.increaseDamage(); break;
                        case ("Coin"): player.collectCoin(); break;
                    }
                    inventoryMap.remove(item);
                }
            }
            }


        for ( Character i : player.movementKeys.keySet() ){
            if( player.movementKeys.get(i) == true ){
                movementCalculation(i);
            }
        }

        player.move(calcX, calcY);


        for ( Terrain terrain : terrains ){
            if(player.boundingBox().isColliding(terrain.boundingBox())){
                resetMovement = true;
            }
        }
        if(resetMovement){
            player.move(-calcX, -calcY);
        }

        calcX=0;
        calcY=0;

    }

    public void movementCalculation(char i) {
        if (i == 'u' && player.movementAbility.get('u'))
            calcY -= 1;
        else if (i == 'l' && player.movementAbility.get('l'))
            calcX -= 1;
        else if (i == 'r' && player.movementAbility.get('r'))
            calcX += 1;
        else if (i == 'd' && player.movementAbility.get('d'))
            calcY += 1;
    }


}
