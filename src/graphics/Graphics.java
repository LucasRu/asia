package graphics;

import entities.Item;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import mechanics.Terrain;
import entities.Model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;


public class Graphics {
    private GraphicsContext gc;
    private Model model;
    // Links static images, facing in different directions
    private Image LinkS = new Image(new FileInputStream("src/PlayerSprites/Knight/idle.gif"));
    private Image LinkN = new Image(new FileInputStream("src/PlayerSprites/Knight/faceNorth.png"));
    private Image LinkE = new Image(new FileInputStream("src/PlayerSprites/Knight/faceEast.png"));
    private Image LinkW = new Image(new FileInputStream("src/PlayerSprites/Knight/faceWest.png"));
    private int interval = 2;
    private int check = 0;
    // Links dynamic images, walking in different directions

    private Image fullHeart = new Image(new FileInputStream("src/PlayerSprites/misc/fullHeart.png"));
    private Image halfHeart = new Image(new FileInputStream("src/PlayerSprites/misc/halfHeart.png"));


    private Image lWalkS = new Image(new FileInputStream("src/PlayerSprites/Knight/runSouth.gif"));
    private Image lWalkN = new Image(new FileInputStream("src/PlayerSprites/Knight/runNorth.gif"));
    private Image lWalkE = new Image(new FileInputStream("src/PlayerSprites/Knight/runEast.gif"));
    private Image lWalkW = new Image(new FileInputStream("src/PlayerSprites/Knight/runWest.gif"));

    // Links dynamic images, attacking in different directions

    private Image lAttackS = new Image(new FileInputStream("src/PlayerSprites/Knight/sliceSouth.gif"));
    private Image lAttackN = new Image(new FileInputStream("src/PlayerSprites/Knight/sliceNorth.gif"));
    private Image lAttackE = new Image(new FileInputStream("src/PlayerSprites/Knight/sliceEast.gif"));
    private Image lAttackW = new Image(new FileInputStream("src/PlayerSprites/Knight/sliceWest.gif"));

    // Dynamic monster image (keese)

    private Image Monster = new Image(new FileInputStream("src/PlayerSprites/Skeleton/Skeleton Idle.gif"));
    private Image MonsterAtt = new Image(new FileInputStream("src/PlayerSprites/Skeleton/Skeleton Attack.gif"));
    private Image MonsterHit = new Image(new FileInputStream("src/PlayerSprites/Skeleton/Skeleton Hit.gif"));
    private Image MonsterDie = new Image(new FileInputStream("src/PlayerSprites/Skeleton/Skeleton Dead.gif"));

    // Static Enviroment TODO: Make it dynamic

    private Image Sample = new Image(new FileInputStream("src/Enviroment/SampleMap.png"));
    // private Image LinkAttack = new Image(new FileInputStream("src/PlayerSprites/LinkA_swordA_south.gif"));

    private Image Potion = new Image(new FileInputStream("src/itemSprites/Potion1.png"));
    private Image Sword = new Image(new FileInputStream("src/itemSprites/Sword.png"));
    private Image Armor = new Image(new FileInputStream("src/itemSprites/Armor.png"));
    private Image Coin = new Image(new FileInputStream("src/itemSprites/Coin.png"));
    private Image GameOver = new Image(new FileInputStream("src/itemSprites/gameOVer.png"));
    private Image YouWon = new Image(new FileInputStream("src/itemSprites/youWon.png"));


    private Image inventory = new Image(new FileInputStream("src/inventory/inventory.png"));


    private int xAbstand = 50;
    private int itemX, itemY;
    public static boolean deleteEnemy = false;


    public Graphics(GraphicsContext gc, Model model) throws FileNotFoundException {
        this.gc = gc;
        this.model = model;
        //this.animationControl = new AnimationControl(-1, false);
        //this.damagedAnimation = new AnimationControl(-1, false);
    }


    public void draw() {
        Random random = new Random();

        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);
        gc.drawImage(Sample, 0, 0, model.WIDTH, model.HEIGHT);
        gc.drawImage(inventory, 1500, 50, 200, 100);

        if (model.getTerrains().isEmpty() && model.getPlayer().getHp() > 0) {
            int spacing = random.nextInt(5);
            gc.drawImage(YouWon, 550 + spacing, 250 + spacing, 800, 400);
        }

        if (model.getPlayer().getHp() <= 0) {
            int spacing = random.nextInt(5);
            gc.drawImage(GameOver, 350 + spacing, 150 + spacing);
        }

        /*for ( Potion potion : model.potions ){
            if(potion.wasPickedUp()){
                gc.drawImage(Potion, potion.getX(), potion.getY(), potion.getW(), potion.getH());
            }
        }*/


        xAbstand = 50;

        for (int i = 1; i <= model.getPlayer().getHp(); i++) {
            if (i == model.getPlayer().getHp() && model.getPlayer().getHp() % 2 != 0) {
                gc.drawImage(halfHeart, 200 + xAbstand, 80, 25, 25);
            } else {
                gc.drawImage(fullHeart, 200 + xAbstand, 80, 25, 25);
                i++;
            }
            xAbstand += 35;
        }

        gc.drawImage(Coin, 550, 70, 40, 40);
        gc.setFont(Font.font("Arial Bold", FontWeight.EXTRA_BOLD, 20));
        gc.fillText(model.getPlayer().getCoinCount(), 600, 100);
        gc.setStroke(Color.WHITE);
        gc.strokeText(model.getPlayer().getCoinCount(), 600, 100);


        int drawState = drawState();
        switch (drawState) {
            case 1:
                gc.drawImage(lAttackE, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                break;
            case 2:
                gc.drawImage(lWalkE, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                break;
            case 3:
                gc.drawImage(LinkE, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                break;
            case 4:
                gc.drawImage(lAttackW, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                playerAttack();
                break;
            case 5:
                gc.drawImage(lWalkW, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                break;
            case 6:
                gc.drawImage(LinkW, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                break;
            case 7:
                gc.drawImage(lAttackN, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                playerAttack();
                break;
            case 8:
                gc.drawImage(lWalkN, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                break;
            case 9:
                gc.drawImage(LinkN, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                break;
            case 10:
                gc.drawImage(lAttackS, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                playerAttack();
                break;
            case 11:
                gc.drawImage(lWalkS, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                break;
            case 12:
                gc.drawImage(LinkS, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
                break;
        }

        List<Terrain> supportList = new ArrayList<>();
        for (Terrain terrain : model.getTerrains()) {
            supportList.add(terrain);
        }

        for (Terrain terrain : supportList) {
            if (terrain.died()) {

                if (!terrain.vanish) {
                    gc.drawImage(MonsterDie, terrain.getX(), terrain.getY(), terrain.getWidth(), terrain.getHeight());
                } else {
                    itemX = terrain.getX();
                    itemY = terrain.getY();
                    model.getTerrains().remove(terrain);
                }


            } else if (terrain.isAttacking) {
                gc.drawImage(MonsterAtt, terrain.getX(), terrain.getY(), 43, 37);
            } else {
                gc.drawImage(Monster, terrain.getX(), terrain.getY(), terrain.getWidth(), terrain.getHeight());
            }
        }

        for (Item item : model.potions.values()) {

            if (model.itemMap.containsKey(item)) {
                switch (model.itemMap.get(item)) {

                    case "Potion":
                        if (!item.wasPickedUp() && item.dropped) {
                            gc.drawImage(Potion, item.getdX(), item.getdY(), item.getdW(), item.getdH());
                        } else if (item.wasPickedUp()) {
                            gc.drawImage(Potion, item.getX(), item.getY(), item.getW(), item.getH());
                        } else {
                            gc.drawImage(Potion, item.getdX(), item.getdY(), item.getInvW(), item.getInvH());
                        }
                        break;

                    case "Weapon":
                        if (!item.wasPickedUp() && item.dropped) {
                            gc.drawImage(Sword, item.getdX(), item.getdY(), item.getdW(), item.getdH());
                        } else if (item.wasPickedUp()) {
                            gc.drawImage(Sword, item.getX(), item.getY(), item.getW(), item.getH());
                        } else {
                            gc.drawImage(Sword, item.getdX(), item.getdY(), item.getInvW(), item.getInvH());
                        }
                        break;

                    case "Armor":
                        if (!item.wasPickedUp() && item.dropped) {
                            gc.drawImage(Armor, item.getdX(), item.getdY(), item.getdW(), item.getdH());
                        } else if (item.wasPickedUp()) {
                            gc.drawImage(Armor, item.getX(), item.getY(), item.getW(), item.getH());
                        } else {
                            gc.drawImage(Armor, item.getdX(), item.getdY(), item.getInvW(), item.getInvH());
                        }
                        break;

                    case "Coin":
                        if (!item.wasPickedUp() && item.dropped) {
                            gc.drawImage(Coin, item.getdX(), item.getdY(), item.getdW(), item.getdH());
                        } else if (item.wasPickedUp()) {
                            gc.drawImage(Coin, item.getX(), item.getY(), item.getW(), item.getH());
                        } else {
                            gc.drawImage(Coin, item.getdX(), item.getdY(), item.getInvW(), item.getInvH());
                        }
                        break;
                }
            }
        }
    }

    public void playerAttack() {


    }

    public int drawState() {

        switch (model.getPlayer().getFacing()) {
            case 'r':
                if (model.getPlayer().movementKeys.get(' ')) {
                    return 1;
                } else if (model.getPlayer().movementKeys.get('r')) {
                    return 2;
                } else {
                    return 3;
                }
            case 'l':
                if (model.getPlayer().movementKeys.get(' ')) {
                    return 4;
                } else if (model.getPlayer().movementKeys.get('l')) {
                    return 5;
                } else {
                    return 6;
                }
            case 'u':
                if (model.getPlayer().movementKeys.get(' ')) {
                    return 7;
                } else if (model.getPlayer().movementKeys.get('u')) {
                    return 8;
                } else {
                    return 9;
                }
            case 'd':
                if (model.getPlayer().movementKeys.get(' ')) {
                    return 10;
                } else if (model.getPlayer().movementKeys.get('d')) {
                    return 11;
                } else {
                    return 12;
                }
        }

        return 0;
    }
}

