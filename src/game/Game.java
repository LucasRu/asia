package game;

import audio.AudioObserver;
import graphics.Graphics;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import entities.Model;
import mechanics.InputHandler;
import mechanics.Timer;

public class Game extends Application {

    private Model model;

    @Override
    public void start(Stage stage) throws Exception {

        AudioObserver audioObserver = new AudioObserver();

        Model model = new Model();
        Canvas canvas = new Canvas(model.WIDTH, model.HEIGHT);
        Group group = new Group();
        group.getChildren().add(canvas);
        Scene scene = new Scene(group);
        stage.setScene(scene);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        Graphics graphics = new Graphics(gc, model);
        Timer timer = new Timer(graphics, model);
        timer.start();
        InputHandler inputHandler = new InputHandler(model);


             // For example



        scene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())
        );
        scene.setOnKeyReleased(
                event -> inputHandler.onKeyReleased(event.getCode())
        );

        stage.show();
        audioObserver.playTheme();
    }


}

